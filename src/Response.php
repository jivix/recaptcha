<?php

declare(strict_types=1);

namespace Jivix\ReCaptcha;

class Response
{
    /**
     * @var bool
     */
    private bool $success;

    /**
     * @var array[string]
     */
    private array $errors;

    /**
     * @var string|null
     */
    private ?string $challengeTimestamp;

    /**
     * @var string|null
     */
    private ?string $hostname;

    /**
     * Response constructor.
     * @param bool $success
     * @param array $errors
     * @param string|null $challengeTimestamp
     * @param string|null $hostname
     */
    public function __construct(bool $success, array $errors = [], string $challengeTimestamp = null, string $hostname = null)
    {
        $this->success = $success;
        $this->errors = $errors;
        $this->challengeTimestamp = $challengeTimestamp;
        $this->hostname = $hostname;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return array
     */
    public function getErrorCodes(): array
    {
        return $this->errors;
    }

    /**
     * @return string|null
     */
    public function getChallengeTimestamp(): ?string
    {
        return $this->challengeTimestamp;
    }

    /**
     * @return string|null
     */
    public function getHostname(): ?string
    {
        return $this->hostname;
    }
}
