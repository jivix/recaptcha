<?php

declare(strict_types=1);

namespace Jivix\ReCaptcha;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Throwable;

class Client
{
    /**
     * Google reCAPTCHA API verify endpoint.
     */
    const VERIFY_ENDPOINT = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * @var string
     */
    private string $secretKey;

    /**
     * ReCaptcha constructor.
     * @param string $secretKey
     */
    public function __construct(string $secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @param string $responseToken The response token provided by reCAPTCHA.
     * @param string|null $remoteIp The remote IP address.
     * @return Response
     */
    public function verify(string $responseToken, string $remoteIp = null): Response
    {
        try {
            $client = new HttpClient();

            $response = $client->request('POST', self::VERIFY_ENDPOINT, [
                'form_params' => [
                    'secret' => $this->secretKey,
                    'response' => $responseToken,
                    'remoteip' => $remoteIp
                ]
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            
            return new Response(
                $data['success'] ?? false,
                $data['error-codes'] ?? [],
                $data['challenge_ts'] ?? null,
                $data['hostname'] ?? null
            );
        } catch (Exception $e) {
            return new Response(false, [
                $e->getMessage()
            ]);
        }
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }
}