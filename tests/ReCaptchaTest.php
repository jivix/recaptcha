<?php

declare(strict_types=1);

namespace Jivix\ReCaptcha\Tests;

use Jivix\ReCaptcha\Client;
use PHPUnit\Framework\TestCase;

class ReCaptchaTest extends TestCase
{
    public function testCanBeCreated()
    {
        $reCaptcha = new Client('');

        $this->assertEquals('', $reCaptcha->getSecretKey());
    }
}